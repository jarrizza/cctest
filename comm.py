import json
import urllib.request


# --------------------
class Comm:
    # --------------------
    def __init__(self):
        self._login_ticket = None
        self._revdata = None

        with open('ini.json') as fp:
            self._ini = json.load(fp)

    # --------------------
    def init(self, revdata):
        self._revdata = revdata
        if self._login_ticket is None:
            self._get_login_ticket()

    # --------------------
    def get_review_info(self, revid):
        cmd = [
            {
                'command': 'SessionService.authenticate',
                'args': {
                    'login': self._ini['userid'],
                    'ticket': self._login_ticket,
                }
            },
            {
                'command': 'ReviewService.getVersions',
                'args': {
                    'reviewId': str(revid)
                }
            },
            {
                'command': 'ReviewService.findReviewById',
                'args': {
                    'reviewId': str(revid)
                }
            },
        ]
        rsp = self._send_cmd(cmd)

        # assume: metadata[revid] is created

        if len(rsp) < 1:
            self._revdata.set_failed_status(revid, rsp[1]['errors'][0]['message'])
            return None, None

        if 'errors' in rsp[1]:
            self._revdata.set_skipped_status(revid, rsp[1]['errors'][0]['message'])
            return None, None

        return rsp[2]['result'], rsp[1]['result']

    # --------------------
    def _get_login_ticket(self):
        cmd = [{
            'command': 'SessionService.getLoginTicket',
            'args': {
                'login': self._ini['userid'],
                'password': self._ini['password']
            },
        }]

        rsp = self._send_cmd(cmd)
        self._login_ticket = rsp[0]['result']['loginTicket']

    # --------------------
    def _send_cmd(self, cmd):
        url = 'http://10.0.3.3:8000/services/json/v1'

        # Headers to specify the content type and encoding for the query
        headers = {'Content-Type': 'application/json;charset=utf-8'}

        cmd_data = json.dumps(cmd)
        req = urllib.request.Request(url, cmd_data.encode('utf-8'), headers=headers)
        response = urllib.request.urlopen(req)
        rsp1 = response.read()
        rsp2 = rsp1.decode('utf-8')
        rsp = json.loads(rsp2)
        return rsp

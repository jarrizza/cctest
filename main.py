import sys

sys.path.append('.')

from app import App


def main():
    app = App()
    app.init()
    app.run()


main()

# $ git --no-pager log -6 --raw --pretty=oneline --no-abbrev

# 5e4bc130c41384511b0efbeeea1f42c94fb3794d (HEAD -> dev_integration, tag: v0.2.21, tag: MES-541_PLUS_CR_PART_2, origin/dev_integration, origin/dev_aardvark_fixes) Bump version to v0.2.21
# :100644 100644 11c9ffba3e65addafb3ae42b853b4188bd0ebbde ok168 ee4106a2eb3f975ed631c7c29c3185ec4cb706c7 M      Application/Tasks/taskMotorControl.c
# :100644 100644 5f73492373ea4d2333226c9194fb01919a79577a ok168 f4bde25aa7b66cca567e806cda5fcff2e452e2b1 M      Application/Utils/Global.c
# e8c0fcccd398dcd6c58002b846075e5d728aec23 Merge branch 'dev_aardvark' into dev_aardvark_fixes
# ec5ef41667cc25f26b3ecd232860db10ffffcc26 (tag: v0.2.20, origin/dev_MES-543_MES-544) Remove extraneous mailbox message from TRTMNT to BTN
# :100644 100644 f03b1cc3b9dc51af336804e28b73f004b08a1164 ok168 766dfd677e9f36d1a113ac391c3ccaf2d1381d73 M      Application/Tasks/taskTreatment.c
# 9ee12432fdd3aae14c775ae4b2a06aad3e84f818 (tag: MES-543_MES-544_CR) Address MES-543 and MES-544
# :100644 100644 a36a3dbada49ab800e4c54e7f1dcdbf96c340c76 ok165 c47c4a86a49a380bf2b80d5e6fcdbeaa3294fc78 M      Application/Command/protocol.c
# :100644 100644 b3c4b3aa105552b90077b7e45f0bec8bf67cafb4 ok165 73c84d8496d0fa1c0e62e1f8a18db989066e28b3 M      Application/Tasks/taskPost.c
# :100644 100644 9ccbf592a6271822f0ba8b943745b667001e128c ok165 f03b1cc3b9dc51af336804e28b73f004b08a1164 M      Application/Tasks/taskTreatment.c
# :100644 100644 adf29259e4a2516da7e12982c69c3266cb804087 ok165 275ee3af9006af4b3ae371dd5fbf5fa92e40acbd M      Application/Tasks/taskTreatment.h
# :100644 100644 3e82560e082869a4e4caba3a6c9a67e51c936a95 ok165 99f553fb80f8a9a711ca1a3957f0321dda06e750 M      projects/development/MMI-MOD1/main.c
# :100644 100644 7ab7b504fba4764a043c8de7e60a3927dc69aea2 ok168 fd5687f1e5b9ea5bb12f585d748f99d47627f3f4 M      tests/stubs/Tasks/taskButton.c
# :100644 100644 b08c793aa0c911ae9ce6a228131190828cc467a5 ok168 f2dd1cf9b3d0145b19420b7c86830eb0c6e3d9a9 M      tests/stubs/Tasks/taskTreatment.c
# 3f7d7e3daeace7770804a418dc36e9ecb9b0f20b (tag: v0.2.18) Address code review comments
# :100644 100644 8460a68916733fc25005f343c10c01cbbfb29caa bad   38494cf2d37e68aedf3525750e530f864135bb58 M      tests/stubs/HAL-Drivers/halPinsDriverBoard3.h
# :100644 100644 8460a68916733fc25005f343c10c01cbbfb29caa bad   dc02567b594360a3490d966c16619465285f5f97 M      tests/stubs/HAL-Drivers/halPinsDriverBoard4.h
# d4a7d156a767e00e0b8d89f3ea1c32232b4f0c8e (tag: v0.2.19, origin/dev_MES-541_motor_charge_check_fix) Bump version to v0.2.19
# :100644 100644 c68cb1501b3ab40031e98e0668958958c8113d21 ok163 3e82560e082869a4e4caba3a6c9a67e51c936a95 M      projects/development/MMI-MOD1/main.c

# d5a31ef43b8a2e868209141019b7d80ed58a3e49 (HEAD -> master, origin/master, origin/john/PFC-181_check_drts_on_start, origin/HEAD) PFC-181 fix wording; undo private function
# :100644 100644 1582728553df423399c4173eb82ecb2114342d4a bad   3c8c0de1b24e5706ccadc377c36d082121668e59 M      lib/pfc/command.py
# :100644 100644 2e69c7cc14604af05a286ff2a88b9397f2e1b588 ok148 c283c1edd828b70271bfbeee02d15d74d670f80a M      lib/pfc/graphics.py
# :100644 100644 7cd68dd859c1bc976fe5f2f3d7c43788ed03dda2 ok148 9b4fd5684f6ba36ba5b9a8715a603d785bba76bf M      ut/mocks/mock_gui.py
# 44b413fb44a6b15d7af781e9ec823d26689d0a38 PFC-181 fix release notes; fix formatting
# :100644 100644 ebbc56b06bf52343d123acc2abecfa856bfecda2 ok148 0eb85a260858b96dc342253f068a68449caa7c13 M      dev_drts.py
# :100644 100644 b79b6d07862b38b7ebb8862eac155aba2827708a ok148 69d40bdd20ca6da07bb270796bb7df457b3f59f8 M      doc/release_notes.md
# :100644 100644 79c89f8d9010a5900d142fde7bd9dcc1c4368705 ok148 18e2fd093682226ea5811b0a4682cab7158df072 M      lib/pfc/drts_client.py
# :100644 100644 f096ea22146abf8ccdccecd5331c0ffa3e6aa1b3 ok148 b48fdaecb12250574f8aee10fc420dd96da577ca M      lib/pfc/environmental.py
# :100644 100644 a19ae197630f94a27aa9e23f263d8b763c0fa587 ok148 0bebb07f10e59a6f8d62b3575c4786023926c38f M      lib/pfc/logger.py
# :100644 100644 ebc4a493c9ac214663126133503685159b2b19bc ok148 2634d223151e88f300255310a55c3733562a27f0 M      lib/pfc/test_instance.py
# 0fe8a29b0e2531fbdaad28a0d025925c3ddca0d5 PFC-181 fix failing UTs
# :100644 100644 9b4fd5684f6ba36ba5b9a8715a603d785bba76bf ok148 7cd68dd859c1bc976fe5f2f3d7c43788ed03dda2 M      ut/mocks/mock_gui.py
# b63d635ae0fe5ef756cb3f422398220c271871db PFC-181 check drts started on start test
# :000000 100644 0000000000000000000000000000000000000000 ok148 58c7b2c77d0659d0b2ff491a4f516cedb2284c14 A      doc/previous_release_notes.md
# :100644 100644 35ddfac4e65b14ad10ea04c61f9421c25ee2a9f7 BAD   b79b6d07862b38b7ebb8862eac155aba2827708a M      doc/release_notes.md
# :100644 100644 a8eb8b759b73de8c6f8f880f58dde69085709250 ok148 e4e0a05c55cd1c40e60a09346e4c9203f5c27030 M      doc/release_process.md
# :100644 100644 3c8c0de1b24e5706ccadc377c36d082121668e59 ok148 1582728553df423399c4173eb82ecb2114342d4a M      lib/pfc/command.py
# :100644 100644 abffbc2a3655cbe31609fdaf1f5e85bd5145c001 ok148 2e69c7cc14604af05a286ff2a88b9397f2e1b588 M      lib/pfc/graphics.py
# :100644 100644 e6dc8c1b89dfda5c140cc9a7274f8aa8b8e20e20 ok148 7c1f6fc5a4f769ba11d1726f078cd70ce2783ed2 M      lib/tools/release.py
# :100644 100644 7bee5c75b11ae410b911052cd9dd84ced3bafd70 ok148 9b4fd5684f6ba36ba5b9a8715a603d785bba76bf M      ut/mocks/mock_gui.py
# :100644 100644 b22045d7011d0979cce06c5710caa417c76a9829 ok148 6dfc299c690c987cfc11558e0ec7adc92f23bcc3 M      ut/test_pump.py

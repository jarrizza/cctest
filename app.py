import os

from comm import Comm
from review_data import ReviewData


# --------------------
class App:
    # --------------------
    def __init__(self):
        self._outdir = os.path.join('.', 'out')

        ##
        self._git_data = {}

        self._revdata = ReviewData()
        self._comm = Comm()

    # --------------------
    def init(self):
        self._check()

    # --------------------
    def run(self):
        self._revdata.init()
        self._comm.init(self._revdata)

        # TODO run from 0 t0 170
        # TODO save last revid, run from last revid to ???
        # ids = [148, 149]
        # 1 - 169
        # 150 deleted
        ids = [148, 149, 157, 156, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170]
        for revid in ids:
            self._load_rev_data(revid)

        # TODO get before & after sha
        self._revdata.report_rev_data()
        self._revdata.report_sha_data()

        # TODO get git output, and find all SHA pairs not covered
        # git --no-pager log -4 --raw --pretty=oneline --no-abbrev

    # --------------------
    def _load_rev_data(self, revid):
        self._revdata.init_review(revid)

        summary, rsp = self._comm.get_review_info(revid)
        if summary is None:
            return

        self._revdata.set_status(revid, summary['reviewPhase'])

        if rsp is None:
            return

        self._get_cc_data(revid, rsp)

    # --------------------
    def _get_cc_data(self, revid, rsp):
        for changelist in rsp['changelists']:
            self._revdata.add_comment(revid, changelist['commitInfo']['comment'])

            for version in changelist['versions']:
                self._revdata.add_change(revid,
                                         version['scmPath'],
                                         version['action'],
                                         version['scmVersionName']
                                         )

    # --------------------
    def _check(self):
        if not os.path.isdir(self._outdir):
            os.makedirs(self._outdir)

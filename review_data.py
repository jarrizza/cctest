# --------------------
class ReviewData:
    # --------------------
    def __init__(self):
        ## holds review data by review id key
        self._rev_metadata = {}

        ## holds all changes by review id key
        self._rev_data = {}

        ## holds review data with sha as the key
        self._sha_data = {}

    # --------------------
    def init(self):
        pass

    # --------------------
    def init_review(self, revid):
        self._rev_metadata[revid] = {
            'status': 'unknown',
            'comments': {},
        }

        self._rev_data[revid] = []

    # --------------------
    def set_status(self, revid, status):
        self._rev_metadata[revid]['status'] = status

    # --------------------
    def set_failed_status(self, revid, comment):
        self.set_status(revid, 'failed')
        self.add_comment(revid, comment)

    def set_skipped_status(self, revid, comment):
        self.set_status(revid, 'skipped')
        self.add_comment(revid, comment)

    # --------------------
    def add_comment(self, revid, comment):
        self._rev_metadata[revid]['comments'][comment.strip()] = 1

    # --------------------
    def add_change(self, revid, path, action, sha):
        self._rev_data[revid].append({
            'path': path,
            'action': action,
            'sha': sha,
        })

        if sha in self._sha_data:
            print(f"updating: {sha} from {self._sha_data[sha]} to {revid} {path}")
        self._sha_data[sha] = revid

    # --------------------
    def report_rev_data(self):
        for revid in sorted(self._rev_metadata.keys(), reverse=True):
            meta = self._rev_metadata[revid]
            print(f"{revid: <4} {'status': <10} {meta['status']}")
            for comment in meta['comments']:
                print(f"{'  --': <4} {'comment': <10} {comment}")

            if meta['status'] in ['failed', 'SKIPPED', 'CANCELLED']:
                continue

            for change in self._rev_data[revid]:
                print(f"  -- {change['sha']} {change['action']: <9} {change['path']}")

    # --------------------
    def report_sha_data(self):
        for sha, revid in self._sha_data.items():
            print(f"{sha} {revid: <4}")
